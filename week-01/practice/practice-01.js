const students = [];

class Student {
    constructor(name, age, isFemale) {
        this.name = name;
        this.age = age;
        this.isFemale = isFemale;
    }

    getStudent() {
        return {
            name : this.name,
            age : this.age,
            isFemale : this.isFemale
        }
    }
}

class StudentManagement {
    static addStudent(student) {
        if (student instanceof Student) {
            students.push(student.getStudent());
        }
    }

    static getStudents() {
        return students;
    }

    static getStudentByIndex(index) {
        if (Number.isInteger(index) && index < students.length && index >= 0) {
            return students[index];
        }

        return null;
    }

    static getIndexOfStudentByName(name) {
        let student = students.filter(student => (student.name === name));

        return student ? student : null;
    }

    static deleteStudentByIndex(index) {
        if (Number.isInteger(index) && index < students.length && index >= 0) {
            return students.splice(index, 1) !== undefined;
        }

        return false;
    }
}

StudentManagement.addStudent(new Student('An', 18, true))
StudentManagement.addStudent(new Student('Bin', 19, false))
StudentManagement.addStudent(new Student('Chi', 20, true))
StudentManagement.addStudent(new Student('Duyen', 21, true))
StudentManagement.addStudent(new Student('Enstanh', 22, false))

console.log('Students are', StudentManagement.getStudents());

// Expected return `null` when having not a student
// Test get information of the students
console.log('Student at the index `0` is', StudentManagement.getStudentByIndex(0));
console.log('Student at the index `a` is', StudentManagement.getStudentByIndex('a'));
console.log('Student at the index `5` is', StudentManagement.getStudentByIndex(5));
console.log('Student at the index `null` is', StudentManagement.getStudentByIndex(null));

// Test get the student by name of them
console.log('Information of `Duyen` is', StudentManagement.getIndexOfStudentByName('Duyen'));
console.log('Information of `Binh` is', StudentManagement.getIndexOfStudentByName('Binh'));
console.log('Information of `null` is', StudentManagement.getIndexOfStudentByName(null));

// Test delete student
console.log('Deleted student at `5` is OK?', StudentManagement.deleteStudentByIndex(5));
console.log('Deleted student at `2` is OK?', StudentManagement.deleteStudentByIndex(2));
console.log('Deleted student at `c` is OK?', StudentManagement.deleteStudentByIndex('c'));
console.log('Deleted student at `-1` is OK?', StudentManagement.deleteStudentByIndex(-1));
console.log('Deleted student at `null` is OK?', StudentManagement.deleteStudentByIndex(null));

console.log('Students are', StudentManagement.getStudents());