const fs = require('fs');
const express = require('express')
const app = express();
const port = 3000;

function readFileSync(filePath) {
    return fs.readFileSync(filePath)
}

async function readFile(filePath) {
    return fs.readFile(filePath, (error, content) => {
        if (error) {
            throw error
        }

        return content
    })
}

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/write-file', (req, res) => {
    if (!fs.existsSync('data')) {
        fs.mkdirSync('data');
    }

    fs.writeFile('data/message.txt', JSON.stringify({name:"Phong"}), error => {
        if (error) {
            return res.status(417).json({
                message : 'Created file fail',
                error
            });
        }

        return res.json({
            message : 'Created file successful'
        });
    })
})

app.get('/read-file', (req, res) => {
    // try {
    //     let data = JSON.parse(readFile('data/message.txt'))

    //     return res.json({
    //         message : 'Successful',
    //         data
    //     })
    // } catch(error) {
    //     return res.status(500).json({
    //         message : 'Error on server',
    //         error
    //     })
    // }

    readFile('data/message.txt').then(content => {
        return res.send(content)
        let data = JSON.parse(content)

        return res.json({
            message : 'Successful',
            data
        })
    }).catch(error => {
        return res.status(500).json({
            message : 'Error on server',
            error : error.stack
        })
    })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`));