import { Router } from 'express';
import UserController from '../controllers/user.controller';
import validate from 'express-validation';
import validation from '../validation';

const router = new Router();

// Get all users
router.get('/users', UserController.getAll);
router.get('/users/:id', UserController.getOne);
router.get('/users/:name', UserController.getOneByName);
router.get('/users/search', validate(validation.user.search), UserController.search);

router.post('/users', validate(validation.user.add), UserController.add);

router.put('/users/:id', UserController.update);
router.patch('/users/:id', UserController.updatePatch);
router.delete('/users/:id', UserController.delete);

export default router;