import { Router } from 'express';
import GroupController from '../controllers/group.controller';
import validate from 'express-validation';
import validation from '../validation';

const router = new Router();

// Get all users
router.get('/group', GroupController.getAll);
router.get('/group/:id', GroupController.getOne);
router.get('/group/:name', GroupController.getOneByName);
router.get('/group/search', validate(validation.group.search), GroupController.search);

router.post('/group', validate(validation.group.add), GroupController.add);

router.put('/group/:id', GroupController.update);
router.patch('/group/:id', GroupController.updatePatch);
router.delete('/group/:id', GroupController.delete);

export default router;