
import { Router } from 'express';
import RoleController from '../controllers/role.controller';
const router = new Router();

// Get all users
router.get('/roles', RoleController.getAll);
router.post('/roles', RoleController.addRole);

export default router;