import express from 'express';
import bodyParser from 'body-parser';
import connectToDb from './db/connect';
import user from './routes/user.routes';
import role from './routes/role.routes';
import group from './routes/group.routes';
import Validate from 'express-validation';

const server = express();

connectToDb();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({
    extended: false
}));

server.use(user);
server.use(role);
server.use(group);

server.use((error, req, res, next) => {
	return res.status(500).json({
		message: 'System error',
		error: error.stack || error
	});
});

server.listen(3000, () => {
    console.log('Server started at: 3000');
});

