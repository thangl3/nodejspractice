import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let userSchema = new Schema({
    firstName: {
    	type: String,
    	required: [ true, 'firstName is required!' ],
    	maxlength: [ 100, 'Too long text!' ]
    },
    lastName: {
    	type: String,
    	required: [ true, 'lastName is required!' ],
    	maxlength: [ 100, 'Too long text!' ]
    },
    fullName: {
    	type: String,
    	required: [ true, 'fullName is required!' ],
    	maxlength: [ 100, 'Too long text!' ]
    },
    email: {
    	type: String,
    },
    password: {
    	type: String,
    },
    age: {
    	type: Number,
    	max: [ 100, 'Too olds!' ]
    },
    refNames: {
    	type: [String],
    	maxlength: [ 10, 'Too long text!' ],
    	trim: true
    },
    isDeleted: {
    	type: Boolean,
    	default: false
    }
});

let User = mongoose.model('User', userSchema);

export default User;