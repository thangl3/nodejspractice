import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let groupSchema = new Schema({
    name: {
    	type: String,
    	required: [ true, '`name` is required!' ],
    	maxlength: [ 255, '`name` too long!' ]
    },
    lastMessage: {
    	type: String,
    },
    author: {
    	type: ObjectId,
    },
    members: {
    	type: [ObjectId],
    },
    deleteAt: {
    	type: Date
    }
});

let Group = mongoose.model('Group', groupSchema);

export default Group;