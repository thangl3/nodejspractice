import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let roleSchema = new Schema({
    name: String,
    isActive: Boolean
});

let Role = mongoose.model('Role', roleSchema);

export default Role;