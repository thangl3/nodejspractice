import User from '../models/user';

const UserController = {};

UserController.getAll = async (req, res, next) => {
    try {
        await User.find()
        //.sort('-dateAdded')
        .exec((err, users) => {
            if (err) {
                res.status(500).send(err);
            }
            return res.json({
                users,
            });
        });
    } catch (error) {
        return next(error)
    }
};

UserController.getOne = async (req, res, next) => {
    let id = req.params.id;

    try {
        let user = await User.findById(id);

        return res.json({
            isSuccess: user ? true : false,
            user
        });
    } catch (error) {
        error.httpStatusCode = 404
        return next(error)
    }
};

UserController.getOneByName = async (req, res, next) => {
    try {
        let name = req.params.name;

        let query = User.find({fullName : /`${name}`/});

        let users = await query.exec();

        return res.json({
            isSuccess: true,
            users
        });
    } catch(e) {
        let error = new Error('User not found!');
        error.httpStatusCode = 404
        return next(error)
    }
};

UserController.add = async (req, res, next) => {
    try {
        //{ password, refNames, firstName, gender, email, age, lastName, fullName } 
        let userData = {...req.body}
        let user = new User(userData);

        await user.save();

        return res.status(200).json({
            message: 'Create success',
            user: user
        });
    } catch (error) {
        return next(error)
    }
};

UserController.update = async (req, res, next) => {
    try {
        let { refNames, firstName, gender, email, age } = req.body
        let id = req.params.id

        let user = await User.findOneAndUpdate(
            {_id : id},
            { refNames, firstName, gender, email, age }
        );

        return res.json({
            message: 'Update user success!',
            user
        });
    } catch(error) {
        return next(error);
    }
};

UserController.updatePatch = async (req, res, next) => {
    try {
        let id = req.params.id

        let user = await User.findOneAndUpdate(
            {_id : id},
            {...req.body},
        );

        return res.json({
            message: 'Update patch of user success!',
            user
        });
    } catch(error) {
        return next(error)
    }
};

UserController.delete = async (req, res, next) => {
    let id = req.params.id

    try {
        let result = await User.findOneAndUpdate({_id : id}, { isDeleted : true });

        if (result) {
            return res.json({
                message: 'Delete user success!'
            });
        }

        let error = new Error('Cannot delete');
        error.httpStatusCode = 404;
        return next(error);
    } catch (error) {
        return next(error);
    }
};

UserController.search = async (req, res, next) => {

};

export default UserController;
