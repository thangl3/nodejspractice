import Group from '../models/group';

const GroupController = {};

GroupController.getAll = async (req, res, next) => {
    try {
        await Group.find()
        //.sort('-dateAdded')
        .exec((err, group) => {
            if (err) {
                res.status(500).send(err);
            }
            return res.json({
                group,
            });
        });
    } catch (error) {
        return next(error)
    }
};

GroupController.getOne = async (req, res, next) => {
    let id = req.params.id;

    try {
        let group = await Group.findById(id);

        return res.json({
            isSuccess: group ? true : false,
            group
        });
    } catch (error) {
        error.httpStatusCode = 404
        return next(error)
    }
};

GroupController.getOneByName = async (req, res, next) => {
    try {
        let name = req.params.name;

        let query = Group.find({fullName : /`${name}`/});

        let group = await query.exec();

        return res.json({
            isSuccess: true,
            group
        });
    } catch(e) {
        let error = new Error('Group not found!');
        error.httpStatusCode = 404
        return next(error)
    }
};

GroupController.add = async (req, res, next) => {
    try {
        //{ password, refNames, firstName, gender, email, age, lastName, fullName } 
        let userData = {...req.body}
        let group = new Group(userData);

        await group.save();

        return res.status(200).json({
            message: 'Create success',
            group: group
        });
    } catch (error) {
        return next(error)
    }
};

GroupController.update = async (req, res, next) => {
    try {
        let { refNames, firstName, gender, email, age } = req.body
        let id = req.params.id

        let group = await Group.findOneAndUpdate(
            {_id : id},
            { refNames, firstName, gender, email, age }
        );

        return res.json({
            message: 'Update group success!',
            group
        });
    } catch(error) {
        return next(error);
    }
};

GroupController.updatePatch = async (req, res, next) => {
    try {
        let id = req.params.id

        let group = await Group.findOneAndUpdate(
            {_id : id},
            {...req.body},
        );

        return res.json({
            message: 'Update patch of group success!',
            group
        });
    } catch(error) {
        return next(error)
    }
};

GroupController.delete = async (req, res, next) => {
    let id = req.params.id

    try {
        let result = await Group.findOneAndUpdate({_id : id}, { isDeleted : true });

        if (result) {
            return res.json({
                message: 'Delete group success!'
            });
        }

        let error = new Error('Cannot delete');
        error.httpStatusCode = 404;
        return next(error);
    } catch (error) {
        return next(error);
    }
};

GroupController.search = async (req, res, next) => {

};

export default GroupController;
