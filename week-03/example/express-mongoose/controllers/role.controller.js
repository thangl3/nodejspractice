import Role from '../models/Role';

const RoleController = {};

RoleController.getAll = async (req, res) => {
    try {
        await Role.find()
        //.sort('-dateAdded')
        .exec((err, roles) => {
            if (err) {
                res.status(500).send(err);
            }
            return res.json({
                roles,
            });
        });
    } catch (err) {
        return res.status(400).json({
            isSuccess: false,
            message: err.message,
            error: err
        });
    }
};

RoleController.addRole = async (req, res) => {
    try {
        const { name, isActive } = req.body

        const role = new Role({
            name, isActive
        });

        await role.save();

        return res.status(200).json({
            message: 'Create success',
            role: role
        });
    } catch(exception) {
        return res.status(400).json({
            message: 'Create fail',
            error: exception
        });
    }
};

export default RoleController;
