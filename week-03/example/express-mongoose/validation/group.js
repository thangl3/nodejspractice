const Joi = require('joi');

exports.add = {
	body: {
		name: Joi.string().required(),
		author: Joi.string().regex(/[a-zA-Z0-9]{3,30}/).required()
	}
};

exports.search = {
	query: {
		name: Joi.string().required()
	}
};