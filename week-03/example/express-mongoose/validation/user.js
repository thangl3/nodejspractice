const Joi = require('joi');

exports.add = {
	body: {
		email: Joi.string().email().required(),
		password: Joi.string().regex(/[a-zA-Z0-9]{3,30}/).required()
	}
};

exports.search = {
	query: {
		name: Joi.string().required()
	}
};

/*
- validate express and mongoose
- req.query
- catch  error in middleware
- Create model with name Group
+ _id, name(255) lastMessage: ObjectId, author: ObjectId, members: Array of ObjectId, deletedAt: Date
+ CRUD, validate
- Research populate in mongoose -> join, foriegn
*/